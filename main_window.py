#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from aceptado import win_acept
from reset import win_reset
from calculo_ingreso import operaciones
from ejecutar import actualizar
lista_datos = []
print("1", lista_datos)


class Main_control():



    def __init__(self):

        # Preparación de interfaz de glade
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./glades/ventana_acondicionada.glade")

        # Carga de ventana principal para visualización
        self.window = self.builder.get_object("main_window")
        self.window.connect("destroy", Gtk.main_quit)
        self.window.set_default_size(800, 600)

        # Botones de interacción en interfaz
        self.boton_aceptar_m = self.builder.get_object("btn_aceptar")
        self.boton_aceptar_m.connect("clicked", self.aceptado)
        self.boton_reset_m = self.builder.get_object("btn_rechazar")
        self.boton_reset_m.connect("clicked", self.boton_reset)
        self.boton_eject_m = self.builder.get_object("btn_ejecutar")
        self.boton_eject_m.connect("clicked", self.ejecutar)

        # Cuadros de texto
        self.ingreso1 = self.builder.get_object("ingreso1")
        self.ingreso2 = self.builder.get_object("ingreso2")


        # Label para mostrar los resultados de las sumas realizadas
        self.label_resultado = self.builder.get_object("label_resultado")

        self.window.show_all()

    # Ejecución de operaciones solicitadas
    def ejecutar(self, btn = None):

        ingreso1 = main.ingreso1.get_text()
        ingreso2 = main.ingreso2.get_text()

        suma_datos = operaciones(str(ingreso1), str(ingreso2))
        lista_datos = actualizar(lista_datos, ingreso1, ingreso2, suma_datos)

        self.label_resultado.set_text(str(suma_datos))


    # Opción aceptar (muestra de datos ingresados)

    def aceptado(self, btn = None):

        accion_a = win_acept()


    # Opción de eliminar datos ingresados

    def boton_reset(self, btn = None):

        accion_r = win_reset()

        self.ingreso1.set_text("")

        pass
        #btn_ingreso1 = self.builder.get_object("")
        #btn_ingreso1 = self.builder.get_object("")



if __name__ == "__main__":
    main = Main_control()
    Gtk.main()
    print("3", lista_datos)
