#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class win_acept():


    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file("./glades/ventana_acondicionada.glade")

        self.dialogo = self.builder.get_object("win_acept")
        self.dialogo.set_title("Entrega de datos ingresados")
        self.dialogo.resize(500, 400)
        # Datos recolectados para imprimir

        # Interacción con el único boton existente
        self.boton_aceptar2 = self.builder.get_object("btn_aceptar2")
        self.boton_aceptar2.connect("clicked", self.aceptado_fin)

        self.label_1 = self.builder.get_object("label_dato_1")
        self.label_2 = self.builder.get_object("label_dato_2")

        # Llamada a lista de datos para indicar datos ingresados


        print("2", lista_datos)

        if len(lista_datos) == 0:
            self.label_1.set_text("No hay datos que mostrar")
            self.label_2.set_text("No hay datos que mostrar")
        elif len(lista_datos) > 2:
            self.label_1.set_text(str(lista_datos[0]))
            self.label_2.set_text(str(lista_datos[1]))

        # Muestra de ventana
        self.dialogo.show_all()

    # Fin de la ventana de información
    def aceptado_fin(self, btn = None):

        self.dialogo.hide()
