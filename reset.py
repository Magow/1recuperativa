
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class win_reset():


    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file("./glades/ventana_acondicionada.glade")

        self.dialogo = self.builder.get_object("win_reset")
        self.dialogo.set_title("Limpieza de datos")
        self.dialogo.resize(500, 400)

        # Botones para interacción
        self.btn_reset_acept = self.builder.get_object("btn_reset_acept")
        self.btn_reset_acept.connect("clicked", self.aceptado_fin)
        self.btn_reset_cancel = self.builder.get_object("btn_reset_cancel")
        self.btn_reset_cancel.connect("clicked", self.reset_fin)

        # Muestra de ventana
        self.dialogo.show_all()

    # Fin de la ventana de información eliminando o dejando los datos
    def aceptado_fin(self, btn = None):
        self.dialogo.destroy()

    def reset_fin(self, btn = None):
        self.dialogo.hide()
